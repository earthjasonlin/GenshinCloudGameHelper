# Genshin Cloud Game Helper

![Genshin Cloud Game Helper](https://socialify.git.ci/earthjasonlin/GenshinCloudGameHelper/image?forks=1&language=1&name=1&owner=1&stargazers=1&theme=Light)

[《云·原神》](https://mhyy.mihoyo.com/)自动签到脚本

**⚠️ 请不要进行宣传，谢谢！一旦发现宣传就跑路！**

这是一个可以帮助你每天自动进行云原神签到的脚本，自动获取每日的 15 分钟（600 分钟后无法获取）

## 赞助

点击下面的 Badge 其中一个就可以跳转到相应页面，感谢老板的支持！

<a href="https://afdian.net/a/earthjasonlin"><img src="https://img.shields.io/badge/%E7%88%B1%E5%8F%91%E7%94%B5-earthjasonlin-%238e8cd8?style=for-the-badge" alt="前往爱发电赞助" width=auto height=auto border="0" /></a> <a href="https://dist.loliquq.cn/d/wechat-reward-code.jpeg"><img src="https://img.shields.io/badge/%E5%BE%AE%E4%BF%A1%E6%94%AF%E4%BB%98-earthjasonlin-%2304BE02?style=for-the-badge" alt="使用微信赞助" width=auto height=auto border="0" /></a>

## 免责声明

一旦你 fork 了本仓库则代表你同意以下内容：

- 所有账号被封禁的情况由使用者自行承担

- 一切由使用本脚本造成的后果由使用者自行承担

## Contributions

[![Contribution](https://repobeats.axiom.co/api/embed/a5d4b41b1ca14049d54b93ad9d00fe478f6154ec.svg "Repobeats analytics image")](#)

## Stargazers over time

[![Stargazers over time](https://starchart.cc/earthjasonlin/GenshinCloudGameHelper.svg)](https://starchart.cc/earthjasonlin/GenshinCloudGameHelper)
